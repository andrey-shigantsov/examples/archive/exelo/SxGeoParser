/* 
 * File:   dbReadExt.h
 */

#ifndef SXGEOPARSER_DBREADEXT_H
#define SXGEOPARSER_DBREADEXT_H

#include "dbRead.h"

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

static inline SxGeoParser_t::errCode_t read_dbFormatDesc(std::FILE * db, SxGeoParser_t::dbHeader_t & dbHeader)
{
  SxGeoParser_t::errCode_t res;
  res = db_read<uint16_t,order::big,16>(db, dbHeader.frmtDescSize);
  if (res)
    return res;
  
  char buf[dbHeader.frmtDescSize];  
  res = db_read(db, buf, dbHeader.frmtDescSize);
  if (res)
    return res;
 
  std::size_t counter = 0;
  while (counter < dbHeader.frmtDescSize)
  {
    char * p = &buf[counter];
    std::size_t len = std::strlen(p);
    dbHeader.frmtDesc.push_back(std::string(p, len));
    counter += len+1;
  }
  return SxGeoParser_t::errNone; 
}

static inline SxGeoParser_t::errCode_t read_dbRange(std::FILE * db, SxGeoParser_t::dbRange_t & data, int8_t geoIdSize)
{
  SxGeoParser_t::errCode_t res;
  
  res = db_read<uint32_t,order::big,24>(db, data.addr);
  if (res)
    return res;
  
  switch(geoIdSize)
  {
  default:
    assert(0);
    break;
    
  case 1:
    res = db_read<uint32_t,order::big,8>(db, data.geoID);
    if (res) return res;
    break;
    
  case 3:
    res = db_read<uint32_t,order::big,24>(db, data.geoID);
    if (res) return res;
    break;
  }  
  return SxGeoParser_t::errNone;
}

template <typename T, typename T2>
static inline T2 db_qN_to_float(T q, int8_t N)
{
  T2 x = q;
  while(N--)
    x /= 10.0;
  return x;
}

static inline SxGeoParser_t::errCode_t db_read_uni(std::FILE * db, std::string type, SxGeoUniFormatVariant_t & val)
{
  SxGeoParser_t::errCode_t res;
  switch(type.at(0))
  {
  case 't':{
    res = db_read<int8_t,8>(db, val);
    break;}
  case 'T':
    res = db_read<uint8_t,8>(db, val);
    break;
    
  case 's':
    res = db_read<int16_t,16>(db, val);
    break;
  case 'S':
    res = db_read<uint16_t,16>(db, val);
    break;
    
  case 'm':
    res = db_read<int32_t,24>(db, val);
    break;
  case 'M':
    res = db_read<uint32_t,24>(db, val);
    break;
    
  case 'i':
    res = db_read<int32_t,32>(db, val);
    break;
  case 'I':
    res = db_read<uint32_t,32>(db, val);
    break;
    
  case 'f':
    res = db_read<int32_t,32>(db, val);
    break;
  case 'd':
    res = db_read<int64_t,64>(db, val);
    break;
    
  case 'n':{
    int n = std::stoi(type.substr(1));      
    int16_t x;
    res = db_read<int16_t,order::little,16>(db, x);
    val = db_qN_to_float<int16_t,float>(x, n);
    break;}
  case 'N':{
    int n = std::stoi(type.substr(1));
    int32_t x;
    res = db_read<int32_t,order::little,32>(db, x);
    val = db_qN_to_float<int32_t,double>(x, n);
    break;}
    
  case 'c':{
    int n = std::stoi(type.substr(1));
    char buf[n+1];
    buf[n] = '\0';
    res = db_read(db, buf, n);
    val = std::string(buf);
    break;}
    
  case 'b':
    char c = 0;
    std::string s;
    do
    {
      res = db_read(db, &c, 1);
      if (res) break;
      if (c == '\0') break;
      s.push_back(c);
    } while (c != '\0');
    val = s;
    break;
  }
  return res;
}

typedef std::map<std::string, SxGeoUniFormatVariant_t> SxGeoUniFormatPDU_t;
static inline SxGeoParser_t::errCode_t read_dbUniFormatPDU(std::FILE * db, std::string format, SxGeoUniFormatPDU_t & pdu)
{
  SxGeoParser_t::errCode_t res;
  
  std::vector<std::string> vars;
  boost::split(vars, format, boost::is_any_of("/"));
  
  for (int i = 0; i < vars.size(); ++i)
  {
    std::vector<std::string> params;
    boost::split(params, vars[i], boost::is_any_of(":"));
    
    res = db_read_uni(db, params[0].data(), pdu[params[1].data()]);
    if (res) break;
  }
  
  return res;
}

//#define SXGEOPARSER_CATALOG_FILE_OUT_ENABLE

typedef SxGeoParser_t::errCode_t (*SxGeoUniFormatPDU_handler_t)(void * targetStruct, const std::fpos_t seek, const SxGeoUniFormatPDU_t * pdu);
static inline SxGeoParser_t::errCode_t read_dbCatalog
(
  std::FILE * db,
  uint32_t catalogSize,
  void * targetStruct, std::string uniFormat,
  SxGeoUniFormatPDU_handler_t pdu_handler
)
{
  SxGeoParser_t::errCode_t res;
 
#ifdef SXGEOPARSER_CATALOG_FILE_OUT_ENABLE
  std::string dbgFileName = "catalog(" + std::to_string(catalogSize) + ").txt";
  std::ofstream dbgFile(dbgFileName, std::ios_base::out);
  if (!dbgFile.is_open())
    std::cerr << "dbgFile not open: " << std::strerror(errno) << "...";
#endif
  
  std::fpos_t db_pos0, db_pos = 0, seek = 0;
  std::fgetpos(db, &db_pos0);
  while (seek < catalogSize)
  {
    SxGeoUniFormatPDU_t pdu;
    res = read_dbUniFormatPDU(db, uniFormat, pdu);
    if (res)
      return res;
        
    assert(pdu_handler);
    res = pdu_handler(targetStruct, seek, &pdu);
    if (res)
      return res;
    
#ifdef SXGEOPARSER_CATALOG_FILE_OUT_ENABLE
    bool unival_ok;
    dbgFile << "name: " << getDbUniVal<std::string>(pdu["name_en"], &unival_ok) << std::endl;
#endif
    std::fgetpos(db, &db_pos);
    seek = db_pos - db_pos0;
  }
  assert(seek == catalogSize);
  return res;
}

#endif /* SXGEOPARSER_DBREADEXT_H */

