/* 
 * File:   SxGeoParser.cpp
 */

#include "SxGeoParser.h"

#include <iostream>
#include <map>

#include <cstdio>
#include <cstring>
#include <ctime>

#include <boost/asio/ip/address_v4.hpp>

#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/compare.hpp>

SxGeoParser_t::SxGeoParser_t(std::string db_filename)
{
  flag_dbIsValid = false;
  
  seekIdx_first = 0;
  seekIdx_main = 0;
  seekRanges = 0;
  seekCountries = 0;
  seekRegions = 0;
  seekCities = 0;
  
  err = open_db(db_filename);
}

SxGeoParser_t::~SxGeoParser_t()
{
}

std::string SxGeoParser_t::errorString()
{
  switch(err)
  {
  case errNone:
    return "no errors";
  case errGeneral:
    return "general failure";
    
  case errFileOpen:
    return "file open failure";
  case errRead:
    return "file read failure";
  case errCheck_dbId:
    return "db check ID failure";
  case errParse_dbUniFormat:
    return "db uni format parse failure";
  case errParse_dbUniFormatPDU:
    return "db uni format pdu parse failure";
    
  default:
    return "unknown error (" + std::to_string(err) + ")";
  }
}

std::string SxGeoParser_t::dbVersionStr()
{
  if (!dbHeader.isValid) return "db invalid";
  
  return std::to_string(dbHeader.ver/10) + "." + std::to_string(dbHeader.ver%10);
}

std::string SxGeoParser_t::dbTypeStr()
{
  if (!dbHeader.isValid) return "db invalid";
  
  switch(dbHeader.type)
  {
  default:
    return "unknown (" + std::to_string(dbHeader.type) + ")";
  case dbUniversal:
    return "Universal";
  case dbCountry:
    return "Country";
  case dbCity:
    return "City";
  case dbCountryIp:
    return "CountryIp";
  case dbCityIp:
    return "CityIp";
  case dbIpgeobase:
    return "ipgeobase";
  }
}

std::string SxGeoParser_t::dbEncodingStr()
{
  if (!dbHeader.isValid) return "db invalid";
  
  switch(dbHeader.encoding)
  {
  default:
    return "unknown (" + std::to_string(dbHeader.encoding) + ")";
  case dbUtf8:
    return "UTF-8";
  case dbLatin1:
    return "Latin1";
  case dbCP1251:
    return "CP1251";
  }
}

std::string SxGeoParser_t::dbCreateDate()
{
  if (!dbHeader.isValid) return "db invalid";
  time_t t = dbHeader.createTimestamp;
  char buf[100];
  std::strftime(buf, 100, "%d.%m.%y", std::localtime(&t));
  std::string str;
  str.append(buf);
  return str;
}

uint32_t SxGeoParser_t::geoIdByAddr(uint32_t addr, bool * ok0)
{
  if (!flag_dbIsValid) return 0;
  
  bool ok;
  
  ok = false;
  long rangeI = 0;
  for (int i = 0; i < dbIdx_main.size(); ++i)
    if (addr < dbIdx_main[i])
    {
      ok = true;
      rangeI = (long)i*dbHeader.blCount;
      std::cout << "SxGeoParser: " 
        << boost::asio::ip::address_v4(addr).to_string()
        << " < " 
        << boost::asio::ip::address_v4(dbIdx_main[i]).to_string()
      ;
      break;
    }
  
  if (!ok) return 0;
  
  long rangeMax = rangeI + dbHeader.blCount;
  if (rangeMax > dbRanges.size()) rangeMax = dbRanges.size();
  
  ok = false;  
  uint32_t id = 0;
  uint32_t addr_without_first = addr & 0x00ffffff;
  for (long i = rangeI+1; i < rangeMax-1; ++i)
    if ((addr_without_first >= dbRanges[i].addr)
         &&(addr_without_first < dbRanges[i+1].addr))
    {
      ok = true;
      id = dbRanges[i].geoID;
      std::cout << ", equal " << boost::asio::ip::address_v4(dbRanges[i].addr).to_string();
      break;
    }
  std::cout << std::endl;
  
  if (ok0) *ok0 = ok;
  return id;
}

SxGeoParser_t::dbCity_t SxGeoParser_t::cityBySeek(uint32_t seek, bool * ok)
{
  for (std::vector<dbCity_t>::iterator it = dbCities.begin(); it != dbCities.end(); ++it)
  {
    if (it->seek == seek)
    {
      if (ok) *ok = true;
      return *it;
    }
  }
  if (ok) *ok = false;
  return SxGeoParser_t::dbCity_t();
}

SxGeoParser_t::dbRegion_t SxGeoParser_t::regionBySeek(uint32_t seek, bool * ok)
{
  for (std::vector<dbRegion_t>::iterator it = dbRegions.begin(); it != dbRegions.end(); ++it)
  {
    if (it->seek == seek)
    {
      if (ok) *ok = true;
      return *it;
    }
  }
  if (ok) *ok = false;
  return SxGeoParser_t::dbRegion_t();
}

SxGeoParser_t::dbCountry_t SxGeoParser_t::countryById(uint8_t id, bool * ok)
{
  for (std::vector<dbCountry_t>::iterator it = dbCountries.begin(); it != dbCountries.end(); ++it)
  {
    if (it->id == id)
    {
      if (ok) *ok = true;
      return *it;
    }
  }
  if (ok) *ok = false;
  return SxGeoParser_t::dbCountry_t();
}

std::vector<std::string> SxGeoParser_t::location_full(std::vector<std::string> addrs, const char * sep)
{
  std::cout << "SxGeoParser: finding cities with countries..." << std::endl;
  
  std::vector<std::string> res;
  if (!flag_dbIsValid)
  {
    std::cout << "SxGeoParser: db is invalid" << std::endl;    
    return res;
  }
     
  for (int i = 0; i < addrs.size(); ++i)
  {    
    boost::system::error_code ec;
    boost::asio::ip::address_v4 addr = 
      boost::asio::ip::address_v4::from_string(addrs[i], ec);
    if (ec) continue;
      
    bool ok;
    
    ok = false;
    uint32_t geoId = geoIdByAddr(addr.to_ulong(), &ok);
    if (!ok) continue;
    
    uint32_t citySeek = geoId - (seekCities - seekCountries);
    
    ok = false;
    dbCity_t City = cityBySeek(citySeek, &ok);
    if (!ok) continue;

    std::string loc = addrs[i] + sep + City.name_en;
    std::cout << "SxGeoParser: " << addrs[i] << " from " << City.name_en;
    
    ok = false;
    dbRegion_t Region = regionBySeek(City.seekRegion, &ok);
    if (ok)
    {
      loc += sep + Region.name_en;
      std::cout << ", " << Region.name_en;
    }
    
    ok = false;
    dbCountry_t Country = countryById(City.idCountry, &ok);
    if (ok)
    {
      loc += sep + Country.name_en;
      std::cout << ", " << Country.name_en;
    }
    
    res.push_back(loc);
    std::cout << std::endl;
  }
  std::cout << "SxGeoParser: found " << res.size() << " records" << std::endl;
  return res;
}

//--------------------------{ ЧТЕНИЕ БД }---------------------------------------

#include <iostream>
#include <fstream>
#include "internal/dbReadExt.h"

#define RETURN(ec) \
do{ \
  err = ec; \
  return err; \
}while(0)

#define FREE \
do{ \
  std::fclose(db); \
}while(0)

#define FREE_AND_RETURN(ec) \
do{ \
  FREE; \
  RETURN(ec); \
}while(0)

#define FXN_FAIL_HANDLER \
do{ \
  std::cout << "fail" << std::endl; \
}while(0)

#define FXN_AND_RETURN(fxn) \
do{ \
  err = fxn; \
  if(err) \
  { \
    FXN_FAIL_HANDLER; \
    FREE; \
    return err; \
  } \
}while(0)

#define PARSE_UNI_AND_RETURN(x,type,val) \
do{ \
  bool ok; \
  x = getDbUniVal<type>(val, &ok); \
  if (!ok) \
  { \
    return SxGeoParser_t::errParse_dbUniFormatPDU; \
  } \
}while(0)

static SxGeoParser_t::errCode_t dbRegions_UniFormatPDU_handler(std::vector<SxGeoParser_t::dbRegion_t> * This, const std::fpos_t seek, const SxGeoUniFormatPDU_t * pdu)
{
  SxGeoParser_t::dbRegion_t x;
  x.seek = seek;
  for (SxGeoUniFormatPDU_t::const_iterator it = pdu->begin(); it != pdu->end(); ++it)
  {
    if (it->first.find("country_seek") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.seekCountry,uint16_t,it->second);
    }      
    if (it->first.find("id") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.id,uint32_t,it->second);
    }
    else if (it->first.find("iso") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.iso,std::string,it->second);
    }
    else if (it->first.find("name_ru") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_ru,std::string,it->second);
    }
    else if (it->first.find("name_en") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_en,std::string,it->second);
    }
    else continue;
  }
  This->push_back(x);
  return SxGeoParser_t::errNone;
}

static SxGeoParser_t::errCode_t dbCountries_UniFormatPDU_handler(std::vector<SxGeoParser_t::dbCountry_t> * This, const std::fpos_t seek, const SxGeoUniFormatPDU_t * pdu)
{
  SxGeoParser_t::dbCountry_t x;
  x.seek = seek;
  for (SxGeoUniFormatPDU_t::const_iterator it = pdu->begin(); it != pdu->end(); ++it)
  {
    if (it->first.find("id") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.id,uint8_t,it->second);
    }
    else if (it->first.find("iso") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.iso,std::string,it->second);
    }
    else if (it->first.find("lat") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.lat,float,it->second);
    }
    else if (it->first.find("lon") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.lon,float,it->second);
    }
    else if (it->first.find("name_ru") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_ru,std::string,it->second);
    }
    else if (it->first.find("name_en") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_en,std::string,it->second);
    }
  }
  This->push_back(x);
  return SxGeoParser_t::errNone;
}

static SxGeoParser_t::errCode_t dbCities_UniFormatPDU_handler(std::vector<SxGeoParser_t::dbCity_t> * This, const std::fpos_t seek, const SxGeoUniFormatPDU_t * pdu)
{
  SxGeoParser_t::dbCity_t x;
  x.seek = seek;
  for (SxGeoUniFormatPDU_t::const_iterator it = pdu->begin(); it != pdu->end(); ++it)
  {
    if (it->first.find("region_seek") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.seekRegion,uint32_t,it->second);
    }
    else if (it->first.find("country_id") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.idCountry,uint8_t,it->second);
    }
    else if (it->first.find("id") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.id,uint32_t,it->second);
    }
    else if (it->first.find("lat") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.lat,double,it->second);
    }
    else if (it->first.find("lon") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.lon,double,it->second);
    }
    else if (it->first.find("name_ru") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_ru,std::string,it->second);
    }
    else if (it->first.find("name_en") != std::string::npos)
    {
      PARSE_UNI_AND_RETURN(x.name_en,std::string,it->second);
    }
  }
  This->push_back(x);
  return SxGeoParser_t::errNone;
}

#undef PARSE_UNI_AND_RETURN

SxGeoParser_t::errCode_t SxGeoParser_t::open_db(std::string& db_filename)
{
  err = errNone;
  flag_dbIsValid = false;
  
  try
  {
    std::FILE* db = std::fopen(db_filename.data(), "rb");
    if (!db)
      RETURN(errFileOpen);

    /* Считывание заголовка */
    std::cout << "SxGeoParser: read DB header...";
    FXN_AND_RETURN(check_dbId(db, dbHeader.isValid));
    FXN_AND_RETURN((db_read<uint8_t,order::big,8>(db, dbHeader.ver)));
    FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbHeader.createTimestamp)));
    FXN_AND_RETURN((db_read<uint8_t,order::big,8>(db, dbHeader.type)));
    FXN_AND_RETURN((db_read<uint8_t,order::big,8>(db, dbHeader.encoding)));
    FXN_AND_RETURN((db_read<uint8_t,order::big,8>(db, dbHeader.elCount_first)));
    FXN_AND_RETURN((db_read<uint16_t,order::big,16>(db, dbHeader.elCount)));
    FXN_AND_RETURN((db_read<uint16_t,order::big,16>(db, dbHeader.blCount)));
    FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbHeader.rangesCount)));
    FXN_AND_RETURN((db_read<uint8_t,order::big,8>(db, dbHeader.blIdSize)));
    FXN_AND_RETURN((db_read<uint16_t,order::big,16>(db, dbHeader.recRegionMaxSize)));
    FXN_AND_RETURN((db_read<uint16_t,order::big,16>(db, dbHeader.recCityMaxSize)));
    FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbHeader.ctlgRegionSize)));
    FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbHeader.ctlgCitySize)));
    FXN_AND_RETURN((db_read<uint16_t,order::big,16>(db, dbHeader.recCountryMaxSize)));
    FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbHeader.ctlgCountrySize)));
    FXN_AND_RETURN(read_dbFormatDesc(db, dbHeader));
    std::cout << "ok" << std::endl;

    std::fgetpos(db, &seekIdx_first);    
    /* Считывание индекса первых байт */
    std::cout << "SxGeoParser: read DB index firsts...";
    dbIdx_first.resize(dbHeader.elCount_first);
    for (uint8_t i = 0; i < dbHeader.elCount_first; ++i)
      FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbIdx_first[i])));
    std::cout << "ok" << std::endl;

    std::fgetpos(db, &seekIdx_main);
    /* Считывание основного индекса */
    std::cout << "SxGeoParser: read DB main index...";
    dbIdx_main.resize(dbHeader.elCount);
    for (uint16_t i = 0; i < dbHeader.elCount; ++i)
      FXN_AND_RETURN((db_read<uint32_t,order::big,32>(db, dbIdx_main[i])));
    std::cout << "ok" << std::endl;

    std::fgetpos(db, &seekRanges);
    /* Считывание диапазонов */
    std::cout << "SxGeoParser: read DB ip-ranges...";
    dbRanges.resize(dbHeader.rangesCount);
    for (uint32_t i = 0; i < dbHeader.rangesCount; ++i)
      FXN_AND_RETURN(read_dbRange(db, dbRanges[i], dbHeader.blIdSize));
    std::cout << "ok" << std::endl;
    
    std::fgetpos(db, &seekRegions);
    /* Считывание справочника регионов */
    std::cout << "SxGeoParser: read DB regions catalog...";
    FXN_AND_RETURN(read_dbCatalog(db, dbHeader.ctlgRegionSize, &dbRegions, dbHeader.frmtDesc[1], (SxGeoUniFormatPDU_handler_t)dbRegions_UniFormatPDU_handler));
    std::cout << "ok" << std::endl;

    std::fgetpos(db, &seekCountries);
    /* Считывание справочника стран */
    std::cout << "SxGeoParser: read DB countries catalog...";
    FXN_AND_RETURN(read_dbCatalog(db, dbHeader.ctlgCountrySize, &dbCountries, dbHeader.frmtDesc[0], (SxGeoUniFormatPDU_handler_t)dbCountries_UniFormatPDU_handler));
    std::cout << "ok" << std::endl;

    std::fgetpos(db, &seekCities);
    /* Считывание справочника городов */
    std::cout << "SxGeoParser: read DB cities catalog...";
    FXN_AND_RETURN(read_dbCatalog(db, dbHeader.ctlgCitySize, &dbCities, dbHeader.frmtDesc[2], (SxGeoUniFormatPDU_handler_t)dbCities_UniFormatPDU_handler));
    std::cout << "ok" << std::endl;
    
    flag_dbIsValid = true;
    FREE_AND_RETURN(err);
  }
  catch(const std::exception & ec)
  {
    std::cerr << ec.what() << std::endl;
    assert(0);
    
    if (err == errNone)
      err = errGeneral;
    return err;
  }
}
